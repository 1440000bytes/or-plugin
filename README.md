# OR plugin


This is a bitcoin core plugin which you can run using `-startupnotify` with bitcoind.


- It adds "OR" in the user agent comment.
- Allows you to relay transaction with more than 80 bytes OP_RETURN.
- Automatically add other nodes that are using the same plugin as peers.

