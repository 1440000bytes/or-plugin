import requests
import socket
import struct
import hashlib
import random
import time
import os

def set_max_opreturn(bitcoin_conf_path):
    max_opreturn_config = 'datacarriersize=1000000\n'

    with open(bitcoin_conf_path, 'a') as bitcoin_conf_file:
        bitcoin_conf_file.write(max_opreturn_config)

def set_ua(bitcoin_conf_path):
    ua_config = 'uacomment=OR\n'

    with open(bitcoin_conf_path, 'a') as bitcoin_conf_file:
        bitcoin_conf_file.write(ua_config)

def create_sub_version():
    sub_version = "/Satoshi:26.0.0/"
    return b'\x0F' + sub_version.encode()

def create_network_address(ip_address, port):
    network_address = struct.pack('<Q12sH', random.getrandbits(64),
        bytearray.fromhex("00000000000000000000ffff") + socket.inet_aton(ip_address), port)
    return network_address

def create_message(magic, command, payload):
    checksum = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[0:4]
    return struct.pack('<L12sL4s', magic, command.encode(), len(payload), checksum) + payload

def create_payload_version(peer_ip_address, peer_port):
    version = 70015 
    services = 1
    timestamp = int(time.time())
    addr_local = create_network_address("127.0.0.1", 8333)
    addr_peer = create_network_address(peer_ip_address, peer_port)
    nonce = random.getrandbits(64)
    start_height = 0
    payload = struct.pack('<LQQ26s26sQ16sL', version, services, timestamp, addr_peer,
                          addr_local, nonce, create_sub_version(), start_height)
    return payload

def parse_version_response(response_data):    
    version_payload = response_data[24:124]  
    version_info = struct.unpack('<LQQ26s26sQ16sL', version_payload)
    version, services, timestamp, addr_peer, addr_local, nonce, sub_version, start_height = version_info
    peer_ip = socket.inet_ntoa(addr_peer[12:16])
    return sub_version.decode()

def get_a_records(domain, num_records):
    try:
        addr_info = socket.getaddrinfo(domain, None, socket.AF_INET)
        
        a_records = [info[4][0] for info in addr_info if info[1] == socket.SOCK_STREAM][:num_records]
        
        return a_records
    except socket.gaierror:
        return None

def addnode(ip_address, port):
    url = "http://127.0.0.1:8332/"
    payload = {
        "jsonrpc": "1.0",
        "id": "or",
        "method": "addnode",
        "params": [f"{ip_address}:{port}", "add"]
    }
    headers = {
        'Content-Type': 'text/plain',
        'Authorization': 'Basic dXNlcjpwYXNz'
    }
    response = requests.post(url, headers=headers, json=payload)


if __name__ == '__main__':
    magic_value = 0xd9b4bef9
    buffer_size = 1024
    domain_name = "seed.bitcoin.sipa.be"
    num_a_records = 25  

    bitcoin_conf_path = f'/home/{os.getenv("USER")}/.bitcoin/bitcoin.conf'

    set_max_opreturn(bitcoin_conf_path)
    set_ua(bitcoin_conf_path)

    while True:
        a_records = get_a_records(domain_name, num_a_records)

        for node in a_records:
            try:
                version_payload = create_payload_version(node, 8333)
                version_message = create_message(magic_value, 'version', version_payload)

                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(20)
                s.connect((node, 8333))

                s.send(version_message)
                response_data = s.recv(buffer_size)
                user_agent = parse_version_response(response_data)

                if "OR" in user_agent:
                    addnode(node, 8333)
                    time.sleep(2)

                print(f"User Agent for {node}:{8333}: {user_agent}")

                s.close()

            except (socket.error, struct.error) as e:
                print(f"Error connecting to {node}:{8333}: {e}")
            except Exception as e:
                print(f"Unexpected error: {e}")

        time.sleep(10)
